package imc.ifsc.edu.br.appimc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class IMCRelativeLayout extends AppCompatActivity {
    EditText etPeso ;
    EditText etAltura ;
    TextView tvIMC ;
    Double imc;
    ImageView magro;
    ImageView normal;
    ImageView sobrepeso;
    ImageView obesidade1;
    ImageView obesidade2;
    ImageView obesidade3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imcrelative_layout);
        //Associação dos atributos da classe java com os Objetos que estão
        this.etPeso = (EditText) findViewById(R.id.EditTextPeso);
        this.etAltura = (EditText)findViewById(R.id.EditTextAltura);
        this.tvIMC = (TextView) findViewById(R.id.TextViewResultadoIMC);
        this.magro = (ImageView)findViewById(R.id.magro);
        this.normal = (ImageView)findViewById(R.id.normal);
        this.sobrepeso = (ImageView)findViewById(R.id.sobrepeso);
        this.obesidade1 = (ImageView)findViewById(R.id.obesidade1);
        this.obesidade2 = (ImageView)findViewById(R.id.obesidade2);
        this.obesidade3 = (ImageView)findViewById(R.id.obesidade3);
    }
    public void calculaIMC(View view) {

        Double peso = Double.parseDouble(this.etPeso.getText().toString());
        Double altura = Double.parseDouble(this.etAltura.getText().toString());
        this.imc=calculaIMC(altura,peso);
        tvIMC.setText(imc.toString());
        if(imc < 18.5){
            magro.setVisibility(View.INVISIBLE);
            normal.setVisibility(View.INVISIBLE);
            sobrepeso.setVisibility(View.INVISIBLE);
            obesidade1.setVisibility(View.INVISIBLE);
            obesidade2.setVisibility(View.INVISIBLE);
            obesidade3.setVisibility(View.INVISIBLE);

            magro.setVisibility(View.VISIBLE);
        }else if(imc > 18.6 && imc < 24.9){
            magro.setVisibility(View.INVISIBLE);
            normal.setVisibility(View.INVISIBLE);
            sobrepeso.setVisibility(View.INVISIBLE);
            obesidade1.setVisibility(View.INVISIBLE);
            obesidade2.setVisibility(View.INVISIBLE);
            obesidade3.setVisibility(View.INVISIBLE);

            normal.setVisibility(View.VISIBLE);
        }else if(imc > 25 && imc < 29.9){
            magro.setVisibility(View.INVISIBLE);
            normal.setVisibility(View.INVISIBLE);
            sobrepeso.setVisibility(View.INVISIBLE);
            obesidade1.setVisibility(View.INVISIBLE);
            obesidade2.setVisibility(View.INVISIBLE);
            obesidade3.setVisibility(View.INVISIBLE);

            sobrepeso.setVisibility(View.VISIBLE);
        }else if(imc > 30 && imc < 34.9){
            magro.setVisibility(View.INVISIBLE);
            normal.setVisibility(View.INVISIBLE);
            sobrepeso.setVisibility(View.INVISIBLE);
            obesidade1.setVisibility(View.INVISIBLE);
            obesidade2.setVisibility(View.INVISIBLE);
            obesidade3.setVisibility(View.INVISIBLE);

            obesidade1.setVisibility(View.VISIBLE);
        }else if(imc > 35 && imc < 39.9){
            magro.setVisibility(View.INVISIBLE);
            normal.setVisibility(View.INVISIBLE);
            sobrepeso.setVisibility(View.INVISIBLE);
            obesidade1.setVisibility(View.INVISIBLE);
            obesidade2.setVisibility(View.INVISIBLE);
            obesidade3.setVisibility(View.INVISIBLE);

            obesidade2.setVisibility(View.VISIBLE);
        }
        else if(imc > 40){
            magro.setVisibility(View.INVISIBLE);
            normal.setVisibility(View.INVISIBLE);
            sobrepeso.setVisibility(View.INVISIBLE);
            obesidade1.setVisibility(View.INVISIBLE);
            obesidade2.setVisibility(View.INVISIBLE);
            obesidade3.setVisibility(View.INVISIBLE);

            obesidade3.setVisibility(View.VISIBLE);
        }
    }

    public double calculaIMC (Double altura, double peso) {
        Double imc = peso /(altura * altura);
        DecimalFormat f = new DecimalFormat("#.####");
        return Double.parseDouble(f.format(imc));
    }


    public void openCreditos(View v ){
        Intent intent =new Intent(this,CreditosActivity.class);
        startActivity(intent);

    }
}
